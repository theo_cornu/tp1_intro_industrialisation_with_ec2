resource "aws_key_pair" "deployer-key" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCpG4/WmlSID0o4gS2NZefgUg9PMqp6lsD6dcChTbSd/YrDfB1PzB+aSm3Mgx6Xz+Xt8j1VVin1IOaN5YhHW5Cve8VMDtm0V/cLP3qn+n/lKBajVAoYwWFaeb0CRlKmydPuC32/iwzp592PqJW3P9sv2UeXgnJUbEshMqFwYekb/RNBb+DdGHzwJ2nuF/PyCW99WP4oF0dJUXbCoAfKeI5/VNSpZKKYFg7cghCL6FW+C8IvJqVVPYwx0rE1RF+aMZ9bQFs7WMTb/HpXUL0CAkHEMQKMvstBZ5Uh2dNzyp4SzG5mwA3wd/aISgHQAEQa3Af3E2yUl1BjLAUZosQy8mxpth/HA0A8Hy0oRmiZRrRH5DM6h15M0FjVhvhrhxnIpfBSlqkFcwKUlNIGBPlLKMX4Z5oINga6piIhyPzQ0+kw37n9rltRy5d2BmvhNKeRK2FiKl2uKx/j+fy1Z9iBEPT8XOzTaH63/TeG2VeNifr+aa+CPVIhHDHUBP3og5QARJE= mariemzyr@MacBook-Pro-de-Marie.local"

}
resource "aws_default_vpc" "default" {
  tags = {
    Name = "deployer-vpc"

  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = "tcp"
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 22
    to_port     = 80
    protocol    = 0
    cidr_blocks = ["172.31.0.0/16"]
  }
}

provider "aws" {
  region = "eu-west-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"
  vpc_security_group_ids = [aws_default_security_group.default.id]
    key_name = "deployer-key"


  tags = {
    Name = "HelloWorld"
  }
}

resource "aws_iam_policy" "permission_boundary_for_delegated_user" {
  name = "permission_boundary_for_demo_delegated_user"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_dynamodb_table" "user_dynamodb_table" {
  name           = "user_table"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "Id"

  attribute {
    name = "Id"
    type = "N"
  }

}

resource "aws_dynamodb_table_item" "sample_data" {
  table_name = "${aws_dynamodb_table.user_dynamodb_table.name}"
  hash_key   = "${aws_dynamodb_table.user_dynamodb_table.hash_key}"

  item = <<EOF
{
  "Id": { "N": "1" },
  "Description": { "S": "Your ID" },
  "Name":{ "S": "1" },
  "Email":{ "S": "1" }

}
EOF
}
